﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Aurora
{
    public partial class Form1 : Form
    {
        readonly BackgroundWorker _server = new BackgroundWorker();

        public Form1()
        {
            InitializeComponent();

            _server.WorkerSupportsCancellation = true;
            _server.WorkerReportsProgress = false;
            _server.DoWork += server_DoWork;
            _server.RunWorkerAsync();
        }

        public void Log(string s)
        {
            listBox1.Items.Add(s);
        }

        void server_DoWork(object sender, DoWorkEventArgs e)
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Server(7413, listBox1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        class Client
        {
            // Отправка страницы с ошибкой
            private static void SendError(TcpClient client, int code)
            {
                // Получаем строку вида "200 OK"
                // HttpStatusCode хранит в себе все статус-коды HTTP/1.1
                var codeStr = code.ToString() + " " + ((HttpStatusCode)code).ToString();
                // Код простой HTML-странички
                var html = "<html><body>" + codeStr + "<hr><strong>Aurora Test Server~ <3</body></html>";
                // Необходимые заголовки: ответ сервера, тип и длина содержимого. После двух пустых строк - само содержимое
                var str = string.Format("HTTP/1.1 {0}\nContent-type: text/html\nContent-Length:{1}\n\n{2}", codeStr, html.Length.ToString(), html);
                // Приведем строку к виду массива байт
                var buffer = Encoding.UTF8.GetBytes(str);
                // Отправим его клиенту
                client.GetStream().Write(buffer, 0, buffer.Length);
                // Закроем соединение
                client.Close();
            }

            // Конструктор класса. Ему нужно передавать принятого клиента от TcpListener
            public Client(TcpClient client)
            {
                // Объявим строку, в которой будет хранится запрос клиента
                var request = "";
                // Буфер для хранения принятых от клиента данных
                var buffer = new byte[1024];
                // Переменная для хранения количества байт, принятых от клиента
                int count;
                // Читаем из потока клиента до тех пор, пока от него поступают данные
                while ((count = client.GetStream().Read(buffer, 0, buffer.Length)) > 0)
                {
                    // Преобразуем эти данные в строку и добавим ее к переменной Request
                    request += Encoding.ASCII.GetString(buffer, 0, count);
                    // Запрос должен обрываться последовательностью \r\n\r\n
                    // Либо обрываем прием данных сами, если длина строки Request превышает 4 килобайта
                    // Нам не нужно получать данные из POST-запроса (и т. п.), а обычный запрос
                    // по идее не должен быть больше 4 килобайт
                    if (request.IndexOf("\r\n\r\n", StringComparison.Ordinal) >= 0 || request.Length > 4096)
                    {
                        break;
                    }
                }

                // Парсим строку запроса с использованием регулярных выражений
                // При этом отсекаем все переменные GET-запроса
                var reqMatch = Regex.Match(request, @"^\w+\s+([^\s\?]+)[^\s]*\s+HTTP/.*|");

                // Если запрос не удался
                if (reqMatch == Match.Empty)
                {
                    // Передаем клиенту ошибку 400 - неверный запрос
                    SendError(client, 400);
                    return;
                }

                // Получаем строку запроса
                var requestUri = reqMatch.Groups[1].Value;

                // Приводим ее к изначальному виду, преобразуя экранированные символы
                // Например, "%20" -> " "
                requestUri = Uri.UnescapeDataString(requestUri);

                /* Если строка запроса оканчивается на "/", то добавим к ней index.html
                if (requestUri.EndsWith("/"))
                {
                    requestUri += "index.html";
                }

                var filePath = "www/" + requestUri;

                // Если в папке www не существует данного файла, посылаем ошибку 404
                if (!File.Exists(filePath))
                {
                    SendError(client, 404);
                    return;
                }

                // Получаем расширение файла из строки запроса
                var extension = requestUri.Substring(requestUri.LastIndexOf('.'));

                // Тип содержимого
                var contentType = "";

                // Пытаемся определить тип содержимого по расширению файла
                switch (extension)
                {
                    case ".htm":
                    case ".html":
                        contentType = "text/html";
                        break;
                    case ".css":
                        contentType = "text/stylesheet";
                        break;
                    case ".js":
                        contentType = "text/javascript";
                        break;
                    case ".jpg":
                        contentType = "image/jpeg";
                        break;
                    case ".jpeg":
                    case ".png":
                    case ".gif":
                        contentType = "image/" + extension.Substring(1);
                        break;
                    default:
                        if (extension.Length > 1)
                        {
                            contentType = "application/" + extension.Substring(1);
                        }
                        else
                        {
                            contentType = "application/unknown";
                        }
                        break;
                }

                // Открываем файл, страхуясь на случай ошибки
                FileStream fs;
                try
                {
                    fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                }
                catch (Exception)
                {
                    // Если случилась ошибка, посылаем клиенту ошибку 500
                    SendError(client, 500);
                    return;
                }

                // Посылаем заголовки
                var headers = "HTTP/1.1 200 OK\nContent-Type: " + contentType + "\nContent-Length: " + fs.Length + "\n\n";
                var headersBuffer = Encoding.ASCII.GetBytes(headers);
                client.GetStream().Write(headersBuffer, 0, headersBuffer.Length);

                // Пока не достигнут конец файла
                while (fs.Position < fs.Length)
                {
                    // Читаем данные из файла
                    count = fs.Read(buffer, 0, buffer.Length);
                    // И передаем их клиенту
                    client.GetStream().Write(buffer, 0, count);
                }

                // Закроем файл и соединение
                fs.Close();

                */

                switch (requestUri)
                {
                    case "/debug:headers":
                        SendTextToUser(client, "HTTP/1.1 200 OK\nContent-Type: text/html\n\n");
                        SendTextToUser(client, request.Replace(Environment.NewLine, "<br/>"));
                        break;
                    case "/debug:about":
                        SendTextToUser(client, "HTTP/1.1 200 OK\nContent-Type: text/html\n\n");
                        SendTextToUser(client, "<body style=\"background:black;color:lime;\">" +
                                               "<strong style=\"color:aqua\">Aurora Repository Server</strong><br>" +
                                               "Made by V01D1nCuB4T0R<br>" +
                                               "E-MAIL: antigreefer226@gmail.com<br>" +
                                               "Skype: antigreefer<br><br>" +
                                               "<strong style=\"color:red\">Aurora <3 you</strong>" +
                                               "</body>");
                        break;
                    default:
                        SRep(requestUri, client);
                        break;
                }

                client.Close();
            }

            private static void SendTextToUser(TcpClient c, string s)
            {
                c.GetStream().Write(Encoding.UTF8.GetBytes(s), 0, s.Length);
            }

            private static void SRep(string r, TcpClient c)
            {
                var error = true;

                if (r.StartsWith("/rep:"))
                {
                    error = false;
                    var reponame = Regex.Split(r, "rep:")[1];

                    if (Directory.Exists("./rep/" + reponame))
                    {
                        //var s = new FileInfo("./rep/"+reponame+"/conf.a");
                        SendTextToUser(c, "HTTP/1.1 200 OK\nContent-Type: text/html\n\n");
                        SendTextToUser(c, "Requested repository: "+reponame+"~ <br>" +
                                          "Repository settings:<br>");

                        var conf = new IniFile("./rep/" + reponame + "/conf.a");
                        var err = false;

                        var rname = string.Empty;
                        try
                        {
                            rname = conf.Read("name", "Repository");
                        }
                        catch
                        {
                            err = true;
                        }

                        if (rname == string.Empty)
                        {
                            err = true;
                        }

                        if (!err)
                        {
                            SendTextToUser(c, "Repository name: "+rname+"<br>");
                            SendTextToUser(c, "File list:<br><br>");
                            var files = Directory.GetFiles("./rep/" + reponame);

                            foreach (var f in files)
                            {
                                var ff = f.Replace('\\', '/');
                                var fff = Regex.Replace(ff, "./rep/", "");
                                SendTextToUser(c, "File: <a href=\"/file:"+fff+"\"> "+fff+"</a><br>");
                            }
                        }
                        else
                        {
                            SendTextToUser(c, "REPOSITORY NOT AVAILABLE. ERROR: No config file/empty repository name!");
                        }

                    } else {
                        SendTextToUser(c, "HTTP/1.1 200 OK\nContent-Type: text/html\n\n");
                        SendTextToUser(c, "REPOSITORY NOT AVAILABLE. ERROR: Repository do not exists");
                    }
                }

                if (r.StartsWith("/file:"))
                {
                    error = false;
                    if (r.Contains("conf.a"))
                    {
                        SendError(c, 403);
                        return;
                    }

                    var filePath = "./rep/"+Regex.Split(r, "file:")[1];

                    var f = new FileInfo(filePath);

                    if (f.Exists)
                    {
                        var extension = filePath.Substring(filePath.LastIndexOf('.'));

                        // Тип содержимого
                        var contentType = "";

                        // Пытаемся определить тип содержимого по расширению файла
                        switch (extension)
                        {
                            case ".htm":
                            case ".html":
                                contentType = "text/html";
                                break;
                            case ".css":
                                contentType = "text/stylesheet";
                                break;
                            case ".js":
                                contentType = "text/javascript";
                                break;
                            case ".jpg":
                                contentType = "image/jpeg";
                                break;
                            case ".jpeg":
                            case ".png":
                            case ".gif":
                                contentType = "image/" + extension.Substring(1);
                                break;
                            default:
                                if (extension.Length > 1)
                                {
                                    contentType = "application/" + extension.Substring(1);
                                }
                                else
                                {
                                    contentType = "application/unknown";
                                }
                                break;
                        }

                        // Открываем файл, страхуясь на случай ошибки
                        FileStream fs;
                        try
                        {
                            fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                        }
                        catch (Exception)
                        {
                            // Если случилась ошибка, посылаем клиенту ошибку 500
                            SendError(c, 500);
                            return;
                        }

                        // Посылаем заголовки
                        var headers = "HTTP/1.1 200 OK\nContent-Type: " + contentType + "\nContent-Length: " + fs.Length + "\n\n";
                        var headersBuffer = Encoding.ASCII.GetBytes(headers);
                        c.GetStream().Write(headersBuffer, 0, headersBuffer.Length);

                        int count;
                        var buffer = new byte[fs.Length];

                        // Пока не достигнут конец файла
                        while (fs.Position < fs.Length)
                        {
                            // Читаем данные из файла
                            count = fs.Read(buffer, 0, buffer.Length);
                            // И передаем их клиенту
                            c.GetStream().Write(buffer, 0, count);
                        }

                        // Закроем файл и соединение
                        fs.Close();

                    }
                    else
                    {
                        SendError(c, 404);
                    }
                }

                if (error)
                {
                    SendError(c, 400);
                }
            }
        }

        class Server
        {
            readonly TcpListener _listener; // Объект, принимающий TCP-клиентов
            readonly ListBox _logger;

            private void Log(string s)
            {
                _logger.BeginInvoke((Action)(() =>
                {
                    _logger.Items.Add(s);
                }));
            }

            // Запуск сервера
            public Server(int port, ListBox l)
            {
                _logger = l;

                Log("[OK] Welcome to Aurora Server <3");
                Log("[OK] Checking rep folder..");

                var fs = Directory.GetDirectories("./rep/");
                foreach (var f in fs)
                {
                    Log("[OK] Found repository folder: "+f);
                    Log("[OK] Checking repository folder: "+f+"..");
                    var s = new FileInfo(f+"/conf.a");

                    // ReSharper disable once InvertIf
                    if (!s.Exists)
                    {
                        Log("[ERR] Can't find repository configuration file in: " + f +
                            "! This repository WILL NOT BE available!");
                    }
                    else
                    {
                        var i = new IniFile(s.FullName);
                        // ReSharper disable once PossibleNullReferenceException
                        Log("[OK] Repository name: " + i.Read("name", "Repository"));
                        Log("[OK] Repository state: " + i.Read("state", "Repository"));
                    }
                }

                Log("[OK] Starting server..");

                try
                {
                    _listener = new TcpListener(IPAddress.Any, port); // Создаем "слушателя" для указанного порта
                    _listener.Start(); // Запускаем его
                    Log("[OK] Server started at port: "+port);
                }
                catch (Exception)
                {
                    Log("[ERR] Can't bind the port: " + port);
                    Log("[ERR] Is server already running?");
                }


                // В бесконечном цикле
                while (true)
                {
                    // Принимаем нового клиента
                    var client = _listener.AcceptTcpClient();
                    // Создаем поток

                    var thread = new Thread(ClientThread);
                    // И запускаем этот поток, передавая ему принятого клиента
                    thread.Start(client);
                }
            }

            static void ClientThread(object stateInfo)
            {
                // Просто создаем новый экземпляр класса Client и передаем ему приведенный к классу TcpClient объект StateInfo
                new Client((TcpClient)stateInfo);
            }

            // Остановка сервера
            ~Server()
            {
                // Если "слушатель" был создан
                if (_listener != null)
                {
                    // Остановим его
                    _listener.Stop();
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            e.DrawBackground();

            var isItemSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);
            var itemIndex = e.Index;
            if (itemIndex >= 0 && itemIndex < listBox1.Items.Count)
            {
                var g = e.Graphics;

                var backgroundColorBrush = new SolidBrush(isItemSelected ? Color.Gray : Color.Black);
                g.FillRectangle(backgroundColorBrush, e.Bounds);

                var itemText = listBox1.Items[itemIndex].ToString();
                var itemTextColorBrush = new SolidBrush(Color.White);

                if(itemText.StartsWith("[OK]")) itemTextColorBrush = new SolidBrush(isItemSelected ? Color.Black : Color.LimeGreen);
                if(itemText.StartsWith("[WARN]")) itemTextColorBrush = new SolidBrush(isItemSelected ? Color.Black : Color.Yellow);
                if (itemText.StartsWith("[ERR]")) itemTextColorBrush = new SolidBrush(isItemSelected ? Color.Black : Color.Red);
                
                g.DrawString(itemText, e.Font, itemTextColorBrush, listBox1.GetItemRectangle(itemIndex).Location);

                // Clean up
                backgroundColorBrush.Dispose();
                itemTextColorBrush.Dispose();
            }

            e.DrawFocusRectangle();
            }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public static void ExecuteCommand(string cmd)
        {
            
        }
    }
}
